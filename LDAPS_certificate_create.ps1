<#
Should only be run on an LDAP / Active Directory Server
#>
Add-Type -AssemblyName System.Windows.Forms,System.Web
  
$global:DesktopPath = mkdir "$([Environment]::GetFolderPath("Desktop"))/LDAPS_Certificates"
  
Start-Transcript -Path "$($global:DesktopPath)\log.txt" -Append -NoClobber
  
# This is to make sure the script is running as an Administrator and a Bypass for Execution policy
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
  
# This grabs the PowerShell Version
$psv = Get-Host | Select-Object Version
  
# Get the Domain name
$global:domain = (Get-WmiObject Win32_ComputerSystem).Domain
  
# This grabs the Fully Qualified Domain Name of the Server the script is running on
$global:fqdn = [System.NET.Dns]::GetHostByName($env:computerName).HostName
  
# This is the password that will be used for when Exporting the PFX
$pwdinput = ""
for ($i=0;$i -lt 30;$i+=1) {
    $pwdinput += ("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" | Get-Random)
}
$global:pwd = ConvertTo-SecureString -String $pwdinput -Force -AsPlainText
  
if([System.IO.File]::Exists("$($global:DesktopPath)\PasswordFile.txt")){
  $rannum = Get-Random -Maximum 100
  New-Item -Path $global:DesktopPath -Name "PasswordFile$rannum.txt" -ItemType "file" -Value $pwdinput
}else{
  New-Item -Path $global:DesktopPath -Name "PasswordFile.txt" -ItemType "file" -Value $pwdinput
}
  
# This IF statement is checking to make sure PS version 4 or higher is install, cert module does not exist in earlier versions
if (!($psv.Version -ge "4.0")){
    Write-Host("Need to upgrade Powershell, Please go to https://microsoft.com/en-us/download/details.aspx?id=54616 and download this and install it")
    pause
    exit
}
  
# Checking to see if the user has a Self-Signed CA certificate to be used
$catitle = "Self-Signed CA"
$caquestion = "Do you have a Self-Signed CA for this Domain?"
$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", `
    "There is a Self-Signed Certificate."
$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", `
    "There is not a Self-Signed Certificate."
$cachoice = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
  
$global:cadecision = $Host.UI.PromptForChoice($catitle, $caquestion, $cachoice, 1)
  
if ($cadecision -eq 0){
  $FileSelector = New-Object System.Windows.Forms.OpenFileDialog -Property @{
    Filter = 'CA Cert (*.pfx) | *.pfx'
  }
  $FileSelector.ShowDialog() | Out-Null
  $global:rootCA = $FileSelector.FileName
}
  
[void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
  
  
# Checking to see if there are multiple Domain Controllers to then create mulitple certificates
$title = 'Multiple Domain Controllers'
$msg   = 'Are there multiple Domain Controllers?
Enter a list of server names of each server separated with a comma (No Spaces)
EX: dc01,dc02,dc03'
  
$title    = 'Multiple Domain Controllers'
$question = 'Are there multiple Domain Controllers?'
$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", `
    "There are multiple Domain Controller."
$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", `
    "There is only one Domain Controller."
$choices = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
  
$global:decision = $Host.UI.PromptForChoice($title, $question, $choices, 1)
if ($decision -eq 0) {
  $userInput = [Microsoft.VisualBasic.Interaction]::InputBox($msg, $title)
  $global:data = $userInput.split(",").trim(" ")
}
  
# Function for creating 2012 certs as the base version of Server 2012 uses Powershell 4.0
function Create-Certs($a){
  
    $ca = $a[0] #CA certificate = 1
    $d  = $a[1] #domain name/fqdn
  
    $DNname = new-object -com "X509Enrollment.CX500DistinguishedName.1"
      
  
    if ($ca -eq 1){
        $keyspec = 2
        $DNname.Encode("CN=" + $d, 0)
        $global:CAname = $DNname
    }else{
        $keyspec = 1
        $DNname.Encode("CN=" + $d, 0)
    }
      
    $key = new-object -com "X509Enrollment.CX509PrivateKey.1" -Property @{
        KeySpec = $keyspec
        Length = 4096
        ExportPolicy = $true
        SecurityDescriptor = "D:PAI(A;;0xd01f01ff;;;SY)(A;;0xd01f01ff;;;BA)(A;;0x80120089;;;NS)"
        MachineContext = $true
    }
    $key.Create()
  
    $hashAlgo = New-Object -ComObject "X509Enrollment.CobjectId.1"
    $hashAlgo.InitializeFromAlgorithmName(1, 0, 0, 'SHA256')
      
    $clientauthoid = new-object -com "X509Enrollment.CObjectId.1"
    $clientauthoid.InitializeFromValue("1.3.6.1.5.5.7.3.2")
      
    $serverauthoid = new-object -com "X509Enrollment.CObjectId.1"
    $serverauthoid.InitializeFromValue("1.3.6.1.5.5.7.3.1")
      
    $ekuoids = new-object -com "X509Enrollment.CObjectIds.1"
    $ekuoids.add($clientauthoid)
    $ekuoids.add($serverauthoid)
      
    $ekuext = new-object -com "X509Enrollment.CX509ExtensionEnhancedKeyUsage.1"
    $ekuext.InitializeEncode($ekuoids)
  
    if($ca -eq 1){
        $keyUsageExt = New-Object -ComObject "X509Enrollment.CX509ExtensionKeyUsage.1"
        $keyUsageExt.InitializeEncode(0x86)
    }else{
        $keyUsageExt = New-Object -ComObject "X509Enrollment.CX509ExtensionKeyUsage.1"
        $keyUsageExt.InitializeEncode(0xa0)
    }
  
    if($ca -eq 0){
        $SAN = New-Object -ComObject X509Enrollment.CX509ExtensionAlternativeNames
        $IANs = New-Object -ComObject X509Enrollment.CAlternativeNames
      
        $d | Foreach-Object {
            $IAN = New-Object -ComObject X509Enrollment.CAlternativeName
            $IAN.InitializeFromString(0x3,$_)
            $IANs.Add($IAN) 
        }
      
        $SAN.InitializeEncode($IANs)
    }
  
    $cert = New-Object -ComObject "X509Enrollment.CX509CertificateRequestCertificate.1"
    $cert.InitializeFromPrivateKey(2, $key, "")
    $cert.Subject = $DNname
    if($ca -eq 1){
        $cert.Issuer = $cert.Subject
    }else{
        $cert.Issuer = $global:CAname
    }
      
    $cert.NotBefore = [DateTime]::Now.AddDays(-1)
    $cert.NotAfter = $cert.NotBefore.AddYears(10)
    $cert.X509Extensions.Add($ekuext)
    $cert.X509Extensions.Add($keyUsageExt)
    $cert.HashAlgorithm = $hashAlgo
  
    if($ca -eq 0){
        $cert.X509Extensions.Add($SAN)
        $signerCertificate = New-Object -ComObject "X509Enrollment.CSignerCertificate.1"
        $signerCertificate.Initialize(1,0,4, $global:rootCA)
        $cert.SignerCertificate = $signerCertificate
    }
  
    $cert.Encode()
  
    $enrollment = new-object -com "X509Enrollment.CX509Enrollment.1"
    $enrollment.InitializeFromRequest($cert)
    $certdata = $enrollment.CreateRequest(0)
    $enrollment.InstallResponse(2, $certdata, 0, "")
  
    if($ca -eq 1){
        $global:rootCA=(Get-ChildItem Cert:\LocalMachine\My\ -recurse | Where-Object {$_.Subject -match "CN=" + $d} | Select-Object -Last 1).thumbprint
        #getting rid of the extra certificate that gets put in the intermediate CA folder
        Remove-Item "Cert:\LocalMachine\CA\$rootCA"
  
        Export-Certificate -Cert "Cert:\LocalMachine\My\$($global:rootCA)" -FilePath "$($global:DesktopPath)\ca.crt"
        Export-Pfxcertificate -Cert "Cert:\LocalMachine\My\$($global:rootCA)" -FilePath "$($global:DesktopPath)\ca.pfx" -Password $global:pwd
        Import-PfxCertificate -FilePath "$($global:DesktopPath)\ca.pfx" -Password $global:pwd -CertStoreLocation "Cert:\LocalMachine\Root"
    }else{
        $madeCert = (Get-ChildItem Cert:\LocalMachine\My\ -recurse | Where-Object {$_.Subject -match "CN=" + $d} | Select-Object -Last 1).thumbprint
        if($d -match "$[sw.].*"){
            Export-pfxcertificate -Cert "Cert:\LocalMachine\My\$($madeCert)" -FilePath "$($global:DesktopPath)\sw.pfx" -Password $global:pwd -ChainOption BuildChain
            Remove-Item "Cert:\LocalMachine\My\$($madeCert)"
        }elseif($d -contains $global:fqdn){
            $global:SvrCert = (Get-ChildItem Cert:\LocalMachine\My\ -recurse | Where-Object {$_.Subject -match "CN=" + $d} | Select-Object -Last 1).thumbprint
            Export-pfxcertificate -Cert "Cert:\LocalMachine\My\$($madeCert)" -FilePath "$($global:DesktopPath)\$($d).pfx" -Password $global:pwd -ChainOption BuildChain
        } else {
            Export-pfxcertificate -Cert "Cert:\LocalMachine\My\$($madeCert)" -FilePath "$($global:DesktopPath)\$($d).pfx" -Password $global:pwd -ChainOption BuildChain
            Remove-Item "Cert:\LocalMachine\My\$($madeCert)"
        }
    }
  
}
  
  
if($global:decision -eq 1){ #decision 1 is 'no, we do not have multiple'
    Create-Certs 1, $global:domain
    Create-Certs 0, $global:fqdn
    Create-Certs 0, "sw.$($global:domain)"
    Remove-Item "Cert:\LocalMachine\My\$($global:rootCA)"
}else{
    Create-Certs 1, $global:domain
    $global:data | ForEach-Object {Create-Certs 0, "$_.$global:domain"}
    Create-Certs 0, "sw.$($global:domain)"
    Remove-Item "Cert:\LocalMachine\My\$($global:rootCA)"
}
  
# Import the Client Certificate via Registry as there is no way to directly import it
if ($decision -eq 1) {
  #Set-Location "HKLM:\SOFTWARE\Microsoft\Cryptography\Services\NTDS\SystemCertificates\My\Certificates\"
  while (!(Test-Path "HKLM:\SOFTWARE\Microsoft\Cryptography\Services\NTDS")){
    Write-Host "Please follow these instructions:
    1 - Press Windows key + R
    2 - type in  mmc
    3 - Press Crtl + m
    4 - Click on Certificates then press Add in the center
    5 - Click on Service Accounts then click Next and Next again
    6 - Make sure Active Directory Domain Services is Highlight then click on Finish
    7 - Then Click Okay
    8 - Double-Click on the Certificates on the left panel then click on NTDS\Personal
    9 - Now you can close this window"
    Pause
  }
  Move-Item "HKLM:\SOFTWARE\Microsoft\SystemCertificates\MY\Certificates\$($global:SvrCert)" "HKLM:\SOFTWARE\Microsoft\Cryptography\Services\NTDS\SystemCertificates\My\Certificates"
}
